var
	gulp         = require('gulp'),
	gutil        = require('gulp-util'),
	sass         = require('gulp-ruby-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cssnano      = require('gulp-cssnano'),
	jshint       = require('gulp-jshint'),
	uglify       = require('gulp-uglify'),
	imagemin     = require('gulp-imagemin'),
	rename       = require('gulp-rename'),
	concat       = require('gulp-concat'),
	notify       = require('gulp-notify'),
	cache        = require('gulp-cache'),
	livereload   = require('gulp-livereload'),
	notifier     = require('node-notifier');
	del          = require('del');

/**
 * Compiles sass files to assets/stylesheets
 *
 * @task styles
 * @param styles (string)
 * @param callback (function)
 */
gulp.task('styles', function () {
	return sass('src/stylesheets/app.sass', { style: (gutil.env.type === 'production') ? 'compressed' : 'expanded' })
		.pipe(autoprefixer('last 2 version'))
		.pipe(gulp.dest('assets/stylesheets'))
		.pipe(cssnano())
		.pipe(gulp.dest('assets/stylesheets'))
		.pipe(notify({ message: 'Styles Compiled' }));
});

/**
 * Compiles sass files to assets/stylesheets
 *
 * @task blocks
 * @param blocks (string)
 * @param callback (function)
 */
gulp.task('blocks', function () {
	return sass('src/stylesheets/blocks.sass', { style: (gutil.env.type === 'production') ? 'compressed' : 'expanded' })
		.pipe(autoprefixer('last 2 version'))
		.pipe(gulp.dest('assets/stylesheets'))
		.pipe(cssnano())
		.pipe(gulp.dest('assets/stylesheets'))
		.pipe(notify({ message: 'Blocks Compiled' }));
});

/**
 * Compiles js files to assets/scripts
 *
 * @task scripts
 * @param scripts (string)
 * @param callback (function)
 */
gulp.task('scripts', function () {
	return gulp.src(['src/scripts/vendor/**/*.js', 'src/scripts/**/*.js'])
		.pipe(concat('app.js'))
		.pipe(gulp.dest('assets/scripts'))
		.pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
		.pipe(gulp.dest('assets/scripts'))
		.pipe(notify({ message: 'JS Compiled' }));
});

/**
 * Compresses images and moves them to assets/images
 *
 * @task images
 * @param images (string)
 * @param callback (function)
 */
gulp.task('images', function () {
	return gulp.src('src/images/**/*')
		.pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
		.pipe(gulp.dest('assets/images'))
		.pipe(gutil.env.type === 'production' ? gutil.noop() : notify({ message: 'Image Compressed & Moved' }));
});

/**
 * Cleans the dist asset directories
 *
 * @task clean
 * @param clean (string)
 * @param callback (function)
 */
gulp.task('clean', function () {
	return del(['assets/stylesheets', 'assets/scripts', 'assets/images']);
});

/**
 * Copies font files to the assets/fonts directory
 *
 * @task clean
 * @param clean (string)
 * @param callback (function)
 */
gulp.task('fonts', function () {
	return gulp.src('src/fonts/**/*')
		.pipe(gulp.dest('assets/fonts/'));
});

/**
 * Runs the default gulp task
 *
 * @task default
 * @param default (string)
 * @param callback (function)
 */
gulp.task('default', ['clean'], function () {
    gulp.start('styles', 'blocks', 'scripts', 'images', 'fonts');
});

/**
 * Runs the watch gulp task
 *
 * @task watch
 * @param watch (string)
 * @param callback (function)
 */
gulp.task('watch', function () {
	gulp.watch('src/fonts/**/*', ['fonts']);
	gulp.watch('src/images/**/*', ['images']);
	gulp.watch('src/scripts/**/*.js', ['scripts']);
	gulp.watch('src/stylesheets/**/*.sass', ['styles', 'blocks']);

	// Uncomment the below lines if you want to use LiveReload
	// livereload.listen();
	// gulp.watch(['assets/**']).on('change', livereload.changed);
});

/**
 * Runs the build gulp task and minifies the scripts
 * Make sure to add --type production to minify JS and SASS
 *
 * @task build
 * @param build (string)
 * @param callback (function)
 */
gulp.task('build', ['clean', 'styles', 'blocks', 'scripts', 'images', 'fonts'], function () {
    notifier.notify({ message: 'Yay! Production build of assets completed.' })
});
