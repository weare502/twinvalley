<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php wp_title(''); ?></title>
        <?php wp_head(); ?>

		<!-- Google Adwords / Analytics / order tracking / conversion metrics -->
		<!-- Global site tag (gtag.js) - Google Analytics - Loaded on every page -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5032469-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-5032469-1');
			gtag('config', 'AW-794997237');
		</script>

		<?php // track all calls to phone number ?>
		<script>
			gtag('config', 'AW-794997237/zucBCOaih9sBEPXjivsC', {
			'phone_conversion_number': '800.515.3311'
			});
		</script>

		<?php if( is_page(9) ) :
			// page id 9 is the global order page
			?>
			
			<script>
				function gtag_report_conversion(url) {
				var callback = function () {
					if (typeof(url) != 'undefined') {
					window.location = url;
					}
				};
				gtag('event', 'conversion', {
					'send_to': 'AW-794997237/HZErCOGXqcwBEPXjivsC',
					'event_callback': callback
				});
				return false;
			}
			</script>
		<?php endif; ?>

		<?php if( is_page(994) ) :
			// page id 994 is the TV Everywhere Page
			?>
			
			<script>
				gtag('event', 'conversion', {'send_to': 'AW-794997237/9MS2CNroqMwBEPXjivsC'});
			</script>
		<?php endif; ?>

		<?php // event page conversion tracking ?>
		<?php if( is_page(11) ) : ?>
			<script>
				gtag('event', 'conversion', {'send_to': 'AW-794997237/kugBCP6ch9sBEPXjivsC'});
			</script>
		<?php endif; ?>

		<?php if( is_single(760) ) :
			// post id 760 is the TV Streaming Page
			?>
			<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '227249425023237');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=227249425023237&ev=PageView&noscript=1" /></noscript>
		<?php endif; ?>
    </head>
    <body <?php body_class(); ?>>
        <?php get_template_part('templates/icons'); ?>
        <?php get_template_part('templates/header'); ?>
        <main class="main-content">