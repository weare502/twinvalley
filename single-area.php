<?php
	global $post;
	$area = new TV_Area ($post);
	$price = $area->price;
	set_query_var ('price_id', $price->ID);
?>
<section class="section-container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php
		echo LDC\Models\View::get('templates/marquee', [
			'title'      => get_the_title(),
			'background' => (has_post_thumbnail()) ? get_the_post_thumbnail_url(get_the_ID(), 'full') : sprintf('%s/assets/images/marquee-sample.jpg', TEMPLATEDIR)
		]);
		get_template_part ('partials/area', 'packages');
	?>
	<div class="sub-section">
		<div class="container container-xs">
			<?php the_content(); ?>
		</div>
	</div>
	<?php endwhile; endif; ?>
</section>
