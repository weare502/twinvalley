<header class="main-header">
	<div class="container">
		<div class="row">
			<div class="main-header-left col">
				<div class="main-brand">
					<a href="/">
						<img class="brand-desktop img-fluid" src="<?php echo TEMPLATEDIR; ?>/assets/images/brand.svg" alt="Twin Valley" />
						<img class="brand-mobile img-fluid" src="<?php echo TEMPLATEDIR; ?>/assets/images/brand-horizontal.svg" alt="Twin Valley" />
					</a>
				</div>
				<div class="mobile-nav-trigger">
					<button onclick="document.querySelector('body').classList.toggle('mobile-navigation-visible');">
						<span></span>
					</button>
				</div>
			</div>
			<div class="main-header-right col-9">
				<?php
					wp_nav_menu([
						'menu'            => 'nav',
						'depth'           => 0,
						'fallback_cb'     => 'navbar_nav::fallback',
						'theme_location'  => 'utility',
						'container_class' => 'menu-utility-nav-container',
					]);
					wp_nav_menu([
						'menu'            => 'nav',
						'depth'           => 2,
						'fallback_cb'     => 'navbar_nav::fallback',
						'theme_location'  => 'header',
						'container_class' => 'menu-main-nav-container',
					]);
				?>
			</div>
		</div>
	</div>
</header>

<div class="mobile-navigation">
	<div class="mobile-navigation-inner">
		<div class="container">
			<div class="mobile-nav-header">
				<div class="main-brand">
					<a href="/">
						<img class="img-fluid" src="<?php echo TEMPLATEDIR; ?>/assets/images/brand-horizontal.svg" alt="Twin Valley" />
					</a>
				</div>
				<div class="mobile-nav-trigger mobile-nav-trigger-close">
					<button onclick="document.querySelector('body').classList.toggle('mobile-navigation-visible');">
						<span></span>
					</button>
				</div>
			</div>
			<div class="mobile-nav-body">
				<?php
					wp_nav_menu([
						'menu'            => 'nav',
						'depth'           => 2,
						'fallback_cb'     => 'navbar_nav::fallback',
						'theme_location'  => 'bill_pay',
						'container_class' => '',
					]);
					wp_nav_menu([
						'menu'            => 'nav',
						'depth'           => 1,
						'fallback_cb'     => 'navbar_nav::fallback',
						'theme_location'  => 'header',
						'container_class' => '',
					]);
					wp_nav_menu([
						'menu'            => 'nav',
						'depth'           => 0,
						'menu_class'      => 'mobile-menu',
						'fallback_cb'     => 'navbar_nav::fallback',
						'theme_location'  => 'utility',
						'container_class' => '',
					]);
				?>
			</div>
			<div class="mobile-nav-footer">
				<a class="btn btn-primary" href="#"><span>Call 1.800.515.3311</span></a>
			</div>
		</div>
	</div>
</div>
