<?php
	global $post;

	$areas = get_posts([
		'post_type'      => 'area',
		'posts_per_page' => -1,
		'orderby'        => 'title',
        'order'          => 'ASC',
	]);

	$area  = get_page_by_path($_GET['a'], $output, 'area');
	$area  = new TV_Area ( $area );
?>

<div id="modal-locations" class="modal fade" tabindex="-1" role="dialog">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div>
				<h1>Where do you live?</h1>
				<!-- <a class="btn btn-primary btn-sm" href="#"><span>Select Location</span></a> -->
				<div class="select-wrapper">
					<select id="modal-area" class="form-control" name="a">
						<option>Select Location</option>
						<?php foreach ((array) $areas as $a) : ?>
							<option value="<?php echo $a->post_name; ?>"><?php echo $a->post_title; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
