<div class="marquee<?php echo (isset($class) && strlen($class)) ? ' marquee-alt' : ''; ?>" style="background-image: url('<?php echo $background; ?>')">
	<div class="marquee-medallion">
		<div>
			<h1><?php echo $title; ?></h1>
			<?php if (isset($link) && strlen($link)) : ?>
				<a class="btn btn-primary btn-sm" href="<?php echo $link; ?>"><span><?php echo $linkTitle; ?></span></a>
			<?php endif; ?>
		</div>
	</div>
</div>
