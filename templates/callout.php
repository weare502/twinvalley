<div class="callout text-center">
	<div class="container container-md">
		<h3>Get Connected Today</h3>
		<p>Twin Valley can connect you to the world. With superior Internet speeds, beautiful picture on TV, and crystal clear and reliable phone service, you can stay connected to the world. Yes I know I said the same thing twice, but I am going for word count within the placeholder here.</p>
		<a class="btn btn-primary btn-yellow" href="#"><span>View Pricing &amp; Packages</span></a>
	</div>
</div>
