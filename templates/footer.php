<footer class="main-footer">
	<div class="container">
		<div class="main-footer-header">
			<div class="row align-items-center">
				<div class="footer-brand col-lg-4 col-md-6">
					<img class="img-fluid" src="<?php echo TEMPLATEDIR; ?>/assets/images/brand-horizontal.svg" alt="Twin Valley" />
				</div>
				<div class="footer-phone col-lg-4 col-md-6">
					<strong>Call Toll Free:</strong> 800.515.3311
				</div>
				<div class="footer-socials col-lg-4 col-md-6">
					<a class="icon icon-social" href="<?php the_field ( 'social_facebook', 'options' ); ?>" target="_blank"><svg><use xlink:href="#icon-facebook" /></svg></a>
					<a class="icon icon-social" href="<?php the_field ( 'social_twitter', 'options' ); ?>" target="_blank"><svg><use xlink:href="#icon-twitter" /></svg></a>
					<a class="icon icon-social" href="<?php the_field ( 'social_linkedin', 'options' ); ?>" target="_blank"><svg><use xlink:href="#icon-linkedin" /></svg></a>
					<a class="icon icon-social" href="mailto:<?php the_field ( 'social_email', 'options' ); ?>" target="_blank"><svg><use xlink:href="#icon-email" /></svg></a>
				</div>
			</div>
		</div>
		<div class="main-footer-body">
			<div class="row">
				<div class="footer-locations col-xl-7 col-lg-8">
					<h5>Locations</h5>
					<div class="row">
						<div class="col-md-4">
							<p>618 6th Street<br />Clay Center, Kansas 67432</p>
						</div>
						<div class="col-md-4">
							<p>22 West Spruce Street<br />Miltonvale, Kansas 67466</p>
						</div>
						<div class="col-md-4">
							<p>1880 Kimball Ave Suite 120B<br />Manhattan, Kansas 66502</p>
						</div>
					</div>
				</div>
				<div class="footer-quick-links col-lg-2 col-6 offset-xl-1">
					<h5>Quick Links</h5>
					<?php
						wp_nav_menu([
							'menu'            => 'nav',
							'depth'           => 0,
							'fallback_cb'     => 'navbar_nav::fallback',
							'theme_location'  => 'site_info',
							'container_class' => 'site-info-nav-container',
						]);
					?>
				</div>
				<div class="footer-site-info col-lg-2 col-6">
					<h5>Site Info</h5>
					<?php
						wp_nav_menu([
							'menu'            => 'nav',
							'depth'           => 0,
							'fallback_cb'     => 'navbar_nav::fallback',
							'theme_location'  => 'quick_links',
							'container_class' => 'quick-links-nav-container',
						]);
					?>
				</div>
			</div>
		</div>
		<div class="main-footer-colophon">
			<div class="row">
				<div class="col-md-8">
					<p>Copyright &copy; <?php echo date('Y'); ?> - Twin Valley Communications - All Rights Reserved</p>
				</div>
				<div class="col-md-4 text-right">
					<p>Design &amp; Development by <a class="credit" href="https://weare502.com" target="_blank"></a></p>
				</div>
			</div>
		</div>
	</div>
</footer>
