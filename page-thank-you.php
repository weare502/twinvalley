<section id="thank-you" class="section-container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="page-content page-content-no-marquee">
		<div class="container container-block container-md">
			<div class="row">
				<div class="col-md-5">
					<img class="img-fluid" src="<?php echo TEMPLATEDIR; ?>/assets/images/iphone.png" alt="iPhone" />
				</div>
				<div class="col-md-7 order-first">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<a class="btn btn-primary" href="/"><span>Return to Homepage</span></a>
					&nbsp;&nbsp;
					<a class="btn btn-text" href="/contact">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
	<?php endwhile; endif; ?>
</section>
