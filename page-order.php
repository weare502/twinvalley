<?php

global $post;

$areas = get_posts([
	'post_type'      => 'area',
	'posts_per_page' => -1,
]);

$area  = get_page_by_path($_GET['a'], $output, 'area');
$area  = new TV_Area ( $area );
$price = $area->price;

set_query_var ('price_id', $price->ID);

$post = $area;

setup_postdata ($post);

?>
<section class="section-container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="page-content page-content-no-marquee">
		<div class="container container-block container-md">
			<h2>Order Form</h2>
			<div class="order-form-toggle" data-current-view="service">
				<a class="current-view" href="javascript:void(0)" data-view="service" data-toggle-view>Select Services</a>
				<a class="" href="javascript:void(0)" data-view="contact" data-toggle-view>Contact Info</a>
			</div>
		</div>
		<div class="container container-block container-md">
			<form class="order-form" method="post">
				<div class="order-form-wrapper" data-current-view="service" data-order-form>
					<div data-view="service">
						<div class="order-form-block">
							<div class="select-wrapper">
								<select id="order-area" class="form-control" name="a">
									<option>Select one...</option>
									<?php foreach ((array) $areas as $a) : ?>
										<option value="<?php echo $a->post_name; ?>" <?php selected ($a->post_name, $area->post_name); ?>><?php echo $a->post_title; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<!-- Package Options -->

						<?php get_template_part('partials/order', 'packages'); ?>

						<!-- Upgrade Options -->

						<?php get_template_part('partials/order', 'upgrades'); ?>

						<div class="order-form-block clearfix mb-5">
							<a class="btn btn-text btn-text-back float-left" href="/area/<?php echo $area->post_name; ?>" style="margin-top: 16px;">Back To Location</a>
							<a class="btn btn-primary float-right" href="javascript:void(0)" data-view="contact" data-toggle-view><span>Next Step</span></a>
						</div>

					</div>
					<div data-view="contact">
						<div class="row row-form">
							<div class="col-6">
								<input tyle="text" class="form-control" name="first_name" placeholder="First Name" />
							</div>
							<div class="col-6">
								<input tyle="text" class="form-control" name="last_name" placeholder="Last Name" />
							</div>
						</div>
						<div class="row row-form">
							<div class="col-6">
								<input tyle="text" class="form-control" name="phone" placeholder="Phone" />
							</div>
							<div class="col-6">
								<input tyle="email" class="form-control" name="email" placeholder="Email" />
							</div>
						</div>
						<div class="row row-form">
							<div class="col-12">
								<input tyle="text" class="form-control" name="address_1" placeholder="Address 1" />
							</div>
						</div>
						<div class="row row-form">
							<div class="col-12">
								<input tyle="text" class="form-control" name="address_2" placeholder="Address 2" />
							</div>
						</div>
						<div class="row row-form">
							<div class="col-sm-6 col-sm-city">
								<input tyle="text" class="form-control" name="city" placeholder="City" />
							</div>
							<div class="col-sm-2 col-6">
								<input tyle="email" class="form-control" name="state" placeholder="State" />
							</div>
							<div class="col-sm-4 col-6">
								<input tyle="email" class="form-control" name="zip" placeholder="Zip" />
							</div>
						</div>
						<div class="row row-form">
							<div class="col-12">
								<label>What time of day might work best for your free installation?</label>
								<select class="form-control" name="time_slot">
									<option selected disabled>Time Slot</option>
									<option value="Morning">Morning</option>
									<option value="Afternoon">Afternoon</option>
								</select>
							</div>
						</div>
						<div class="row row-form row-submit">
							<div class="col-12">
								<p>Once you submit your order, one of our customer service agents will be in touch to finalize your order using the information you have provide.</p>
								<p>No payment is due at this time.</p>
								<br />
								<?php // BREAKS FORM REDIRECT: onclick="gtag_report_conversion('https://twinvalley.net/order');" ?>
								<button type="submit" class="btn btn-primary btn-submit"><div class="spinner"></div><span>Submit Order Request</span></button>
							</div>
						</div>
					</div>
				</div>
				<div class="order-form-summary">
					<div class="order-form-summary-container">
						<div class="order-form-summary-inner">
							<div class="summary-block">
								<h2>Your Order</h2>
								<p class="selected-location">Selected Location: <span><?php echo $area->post_name; ?></span></p>
							</div>
							<div class="summary-block" data-packaged-service>
								<h3>Selected Package</h3>
								<ul>
									<li class="no-selection" data-line-item-price="0.00">No package selected</li>
								</ul>
							</div>
							<div class="summary-block" data-upgrade-service>
								<h3>Additional Features</h3>
								<ul>
									<li class="no-selection" data-line-item-price="0.00">No upgrades selected</li>
								</ul>
							</div>
							<div class="summary-block">
								<h3>Order Total<span>Before applicable taxes and fees</span></h3>
								<div class="summary-total">
									<span class="dollars">0</span>
									<sup class="cents">00</sup>
									<!-- <sub>/MO.</sub> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php endwhile; endif; ?>
</section>
