<?php if ( have_rows ( 'upgrades', $price_id ) ) : ?>
<h2>
    Upgrade Options
</h2>
<?php while ( have_rows ( 'upgrades', $price_id ) ) : the_row(); $service = get_sub_field ( 'service' ); $type = get_sub_field ( 'type' ); ?>
<?php if ( have_rows ( 'options', $price_id ) ) : while ( have_rows ( 'options', $price_id ) ) : the_row(); ?>
    <h3>
        <?php the_sub_field ( 'option' ); ?>
    </h3>
    <p>
        <?php the_sub_field ( 'description' ); ?>
    </p>
    <h4>
        <?php echo money_format ( '$%i', get_sub_field ( 'price' ) ); ?>
    </h4>
    <p>
        <?php echo $service->post_title; ?>
        <input name="<?php echo $service->post_title; ?>" type="<?php echo $type; ?>" value="<?php the_sub_field ( 'option' ); ?>">
    </p>
    <?php

    endwhile; endif;

    ?>
    <?php

    endwhile; endif;

    ?>