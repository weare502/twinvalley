<div class="order-form-block order-form-upgrade-block" style="display: none;">
	<h3>Additional Features</h3>
	<?php if (have_rows('upgrades', $price_id)) : while (have_rows('upgrades', $price_id)) : the_row(); $service = get_sub_field('service'); $type = get_sub_field('type'); $name = get_sub_field('name'); ?>
		<?php $count = count(get_sub_field ('options')); ?>
		<?php if ($count > 1) : ?><div class="option-block-group"><?php endif; ?>
		<?php if (have_rows ('options', $price_id)) : while (have_rows('options', $price_id)) : the_row(); ?>
		<div class="order-form-option-block" data-service="<?php echo $service->post_name; ?>" style="display: none;">
			<h4>
				<span><?php echo money_format('+ $%i', get_sub_field('price')); ?></span>
				<?php the_sub_field('option'); ?>
			</h4>
			<div class="row">
				<div class="col-md-8 col-7">
					<p style="margin: 0;"><?php the_sub_field('description'); ?></p>
				</div>
				<div class="col-md-4 col-5">
					<button type="button" class="btn btn-text" data-upgrade-name="<?php echo the_sub_field('option'); ?>" data-upgrade-price="<?php echo get_sub_field('price'); ?>" data-input-type="<?php echo $type; ?>" data-group-name="<?php echo get_sub_field('group_name'); ?>" data-upgrade-select>Add to Order</a>
				</div>
			</div>
		</div>
		<?php endwhile; endif ?>
		<?php if ($count > 1) : ?></div><?php endif; ?>
	<?php endwhile; endif; ?>
</div>
