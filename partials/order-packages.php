<?php

    if (have_rows('categories', $price_id)) : while (have_rows('categories', $price_id)) : the_row(); $services = []; $category = get_sub_field ('category');
?>
    <div class="order-form-block">
        <h3><?php the_sub_field ('category'); ?></h3>
        <p><?php the_sub_field ('description'); ?></p>
        <?php if (have_rows ('packages', $price_id)) : while (have_rows ('packages', $price_id)) : the_row(); $type = get_sub_field('type'); $groupName = get_sub_field('group_name'); ?>
            <div class="order-form-option-block">
                <h4>
                    <span><?php echo money_format('$%i', get_sub_field ('price')); ?></span>
                    <?php the_sub_field ('package'); ?>
                </h4>
                <div class="row">
                    <div class="col-6">
                    <?php
                        if (have_rows('services', $price_id)) {
                            $service_list = [];
                            echo '<ul class="service-list">';
                            while (have_rows ('services', $price_id)) {
                                the_row();

                                $service      = get_sub_field('service');
                                $service_type = get_post($service->post_parent);

                                array_push($service_list, $service_type->post_name);

                                if (!in_array($service_type, $services)) {
                                    $services[] = $service_type;
                                }

                                echo sprintf('<li><strong>%s</strong>: %s</li>', get_the_title($service_type), get_the_title($service));

                            }
                            echo '</ul>';
                        }
                    ?>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-text" data-package-category="<?php echo sanitize_title ( $category ); ?>" data-package-slug="<?php echo sanitize_title ( get_sub_field ( 'package' ) ); ?>" data-package-name="<?php the_sub_field ('package'); ?>" data-package-price="<?php the_sub_field ('price'); ?>" data-input-type="<?php echo $type; ?>" data-service-type="<?php echo implode(',', $service_list); ?>" data-group-name="<?php echo $groupName; ?>" data-package-select>Select</a>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
<?php endwhile; endif; ?>
