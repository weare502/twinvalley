<?php
	global $post;
	$post_slug = $post->post_name;

	$idx = 0;

	if (have_rows('categories', $price_id)) : while (have_rows('categories', $price_id)) : the_row(); $services = []; $idx++;
?>
	<div class="sub-section<?php echo $idx % 2 == 0 ? ' sub-section-yellow': ''; echo $idx == 1 ? ' section-intro' : '' ?> text-center">
		<div class="container container-sm">
			<h2 class="underline mb-5"><?php the_sub_field('category'); ?></h2>
			<p><?php the_sub_field('description'); ?></p>
		</div>
		<?php if (have_rows('packages', $price_id)) : ?>
		<div class="sub-section text-center">
			<div class="container">
				<div class="row justify-content-center">
					<?php
						while (have_rows('packages', $price_id)) : the_row();
						$tagline = get_sub_field( 'tagline' );
						$price = explode('.', get_sub_field('price'));
					?>
					<div class="pricing-block col-lg-auto col-md-6 col-sm-6 col-12">
						<div class="pricing-block-inner">
							<div class="price-heading">
							<?php if( $tagline ) : ?>
								<div class="best-value-marker">
									<?php echo $tagline; ?>
								</div>
							<?php endif; ?>
							</div>
							<div class="price-total">
								<span class="dollars"><?php echo $price[0]; ?></span>
								<sup class="cents"><?php echo $price[1]; ?></sup>
								<!-- <sub>/MO.</sub> -->
							</div>
							<div class="pricing-details">
								<ul>
									<?php
										if (have_rows('services', $price_id)) : while (have_rows('services', $price_id)) : the_row();
										$service      = get_sub_field('service');
										$service_type = get_post($service->post_parent);

										if (!in_array($service_type, $services)) {
											$services[] = $service_type;
										}
									?>
										<li><strong><?php echo get_the_title($service_type); ?></strong><?php echo get_the_title($service); ?></li>
									<?php endwhile; endif; ?>
								</ul>
							</div>
							<div class="pricing-cta">
								<a class="btn btn-primary" href="/order?a=<?php echo $post_slug; ?>&b=<?php echo sanitize_title ( get_sub_field ( 'package' ) ); ?>"><span>Purchase</span></a>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
				</div>

				<?php if (count($services) == 1) : $service = $services[0]; ?>
					<a class="btn btn-text" href="<?php echo get_the_permalink($service); ?>">Learn more about Twin Valley <?php echo get_the_title($service); ?></a>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
<?php endwhile; endif; ?>
