<?php

$id = 'eyebrow-' . $block['id'];
$className = 'eyebrow';
if ( ! empty ( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty ( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
$text = get_field ( 'text' );

?>
<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <?php echo $text; ?>
</div>