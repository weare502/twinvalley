<?php

$id = 'button-' . $block['id'];
$align = '';
if ( ! empty ( $block['align'] ) ) {
    $align = 'align' . $block['align'];
}
$className = 'btn ' . get_field ( 'class' );
$text = get_field ( 'text' );
$url = ! is_admin () ? get_field ( 'url' ) : false;

?>
<div class="<?php echo $align; ?>">
    <a <?php if ( $url ) : ?>href="<?php echo $url; ?>"<?php endif; ?> id="<?php echo $id; ?>" class="<?php echo $className; ?>">
        <span><?php echo $text; ?></span>
    </a>
</div>