<?php

$id = 'testimonial-' . $block['id'];
$className = 'testimonial-block sub-section';
if ( ! empty ( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty ( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
$testimonial = get_field ( 'testimonial' );
$author = get_field ( 'author' );

?>
<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <div class="container container-md">
        <div class="testimonial">
            <div class="testimonial-text">
                <?php echo $testimonial; ?>
            </div>
            <div class="testimonial-author">
                <?php echo $author; ?>
            </div>
        </div>
    </div>
</div>