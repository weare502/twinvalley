<?php

$id = 'team-' . $block['id'];
$className = 'team';
if ( ! empty ( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
$image = get_field ( 'image' );
$name = get_field ( 'name' );
$title = get_field ( 'title' );
$text = get_field ( 'text' );
$phone = get_field ( 'phone' );
$email = get_field ( 'email' );
$follow = get_field ( 'follow' );

?>
<div class="team-member-block row alignwide">
    <div class="team-image col-md-4">
        <img class="img-fluid" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
    </div>
    <div class="col-md-8">
        <h2>
            <?php echo $name; ?>
            <?php if ( $title ) : ?>
            <span>
                <?php echo $title; ?>
            </span>
            <?php endif; ?>
        </h2>
        <p>
            <?php echo $text; ?>
        </p>
        <div class="team-contact-links">
            <div class="row">
                <?php if ( $phone ) : ?>
                <div class="col-4">
                    <a class="call" href="tel:<?php echo preg_replace ( '/[^0-9]/', '', $phone ); ?>">Call</a>
                </div>
                <?php endif; if ( $email ): ?>
                <div class="col-4">
                    <a class="email" href="mailto:<?php echo $email; ?>">Email</a>
                </div>
                <?php endif; if ( $follow ): ?>
                <div class="col-4">
                    <a class="follow" href="<?php echo $follow; ?>" target="_blank">Follow</a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>