<?php

$id = 'service-' . $block['id'];
$className = 'service-block row align-items-center';
if ( ! empty ( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty ( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
$icon = get_field ( 'icon' );
$title = get_field ( 'title' );
$description = get_field ( 'description' );
$url = get_field ( 'url' );
$button = get_field ( 'button' );

?>
<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <?php if ( $icon ): ?>
    <div class="col-md-2">
        <div class="service-icon">
            <?php echo wp_get_attachment_image ( $icon, array ( 100, 100 ), false, array ( 'class' => 'img-fluid' ) ); ?>
        </div>
    </div>
    <?php endif; ?>
    <div class="col-md-<?php echo $icon ? '10' : '12'; ?>">
        <?php if ( $title ) : ?>
        <h3>
            <?php echo $title; ?>
        </h3>
        <?php endif; if ( $description ) : ?>
        <p>
            <?php echo $description; ?>
        </p>
        <?php endif; if ( $url && $button ) : ?>
        <a class="btn btn-text" href="<?php echo $url; ?>">
            <?php echo $button; ?>
        </a>
        <?php endif; ?>
    </div>
</div>