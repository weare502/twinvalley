<?php

$id = 'card-' . $block['id'];
$className = 'card';
if ( ! empty ( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty ( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
$image = get_field ( 'image' );
$title = get_field ( 'title' );
$text = get_field ( 'text' );
$url = ! is_admin () ? get_field ( 'url' ) : false;

?>
<<?php if ( $url ) : ?>a href="<?php echo $url; ?>"<?php else : ?>div<?php endif; ?> id="<?php echo $id; ?>" class="<?php echo $className; ?>">
    <?php if ( $image ) : ?>
    <img src="<?php echo $image['sizes']['medium']; ?>" class="card-img-top" alt="<?php echo $image['sizes']['medium']; ?>">
    <?php endif; ?>
    <div class="card-body">
        <?php if ( $title ) : ?>
        <h4 class="card-title">
            <?php echo $title; ?>
        </h4>
        <?php endif; if ( $text ) : ?>
        <p class="card-text">
            <?php echo $text; ?>
        </p>
        <?php endif; ?>
    </div>
</<?php if ( $url ) : ?>a<?php else : ?>div<?php endif; ?>>
