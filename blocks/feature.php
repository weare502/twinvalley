<?php

$id = 'feature-' . $block['id'];
$className = 'home-services sub-section';
if ( ! empty ( $block['className'] ) ) {
    $className .= ' ' . $block['className'];
}
if ( ! empty ( $block['align'] ) ) {
    $className .= ' align' . $block['align'];
}
$image = get_field ( 'image' );
$excerpt = get_field ( 'excerpt' );
$title = get_field ( 'title' );
$text = get_field ( 'text' );
$button = get_field ( 'button' );
$button_url = get_field ( 'button_url' );
$link = get_field ( 'link' );
$link_url = get_field ( 'link_url' );
$background = get_field ( 'background' );
$reverse = get_field ( 'reverse' );

?>
<div id="<?php echo $id; ?>" class="<?php echo $className; echo $background == 'yellow' ? ' home-services-yellow' : ''; ?>">
    <div class="container container-md">
        <div class="row align-items-center<?php echo $reverse ? ' flex-row-reverse' : ''; ?>">
            <div class="col-md-6 text-center">
                <img class="img-fluid" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
            </div>
            <div class="col-md-6">
                <?php if ( $excerpt ) : ?>
                <span class="eyebrow">
                    <?php echo $excerpt; ?>
                </span>
                <?php endif; if ( $title ) : ?>
                <h2>
                    <?php echo $title; ?>
                </h2>
                <?php endif; if ( $text ) : ?>
                <p>
                    <?php echo $text; ?>
                </p>
                <?php endif; if ( $button && $button_url ) : ?>
                <a class="btn btn-primary<?php echo $background == 'yellow' ? ' btn-yellow' : ''; ?> mr-2" href="<?php echo $button_url; ?>">
                    <span><?php echo $button; ?></span>
                </a>
                <?php endif; if ( $link && $link_url ) : ?>
                <a class="btn btn-text" href="<?php echo $link_url; ?>">
                    <span><?php echo $link; ?></span>
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
