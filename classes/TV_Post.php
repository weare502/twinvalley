<?php

/*
Class
*/

class TV_Post {

  public function __construct ( $post ) {
    if ( ! is_object ( $post ) ) {
      $post = get_post ( $post );
    }
    foreach ( (array) get_object_vars( $post ) as $key => $value ) {
      $this->$key = $value;
    }
  }

  public function __isset ( $key ) {
    return get_field ( $key, $this->ID ) !== false;
  }

  public function __get ( $key ) {
    return get_field ( $key, $this->ID );
  }

}
