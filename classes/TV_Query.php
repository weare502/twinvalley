<?php

class TV_Query extends WP_Query {

  function __construct ( $args = array () ) {
    $args = array_merge ( $args, array (
      'posts_per_page' => -1,
      'no_found_rows' => true,
      'update_post_term_cache' => false,
      'update_post_meta_cache' => false,
    ) );
    parent::__construct ( $args );
  }

}
