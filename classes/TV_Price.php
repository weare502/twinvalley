<?php

/*
Actions
*/

add_action ( 'init', 'tv_price_init' );

function tv_price_init () {
  register_post_type ( 'price', array (
    'labels' => array (
      'name' => 'Price Lists',
      'singular_name' => 'Price List',
      'add_new' => 'Add Price List',
      'add_new_item' => 'Add Price List',
      'edit_item' => 'Edit Price List',
      'new_item' => 'New Price List',
      'view_item' => 'View Price List',
      'view_items' => 'View Price Lists',
      'search_items' => 'Search Price Lists',
      'all_items' => 'Price Lists',
      'featured_image' => 'Price List Photo',
      'set_featured_image' => 'Set Price List Photo',
      'remove_featured_image' => 'Remove Price List Photo',
      'use_featured_image' => 'Use Price List Photo',
      'menu_name' => 'Price Lists',
      'name_admin_bar' => 'Price List',
    ),
    'supports' => array (
      'title',
    ),
    'menu_position' => 4,
    'menu_icon' => 'dashicons-cart',
    'hierarchical' => true,
    'show_in_menu' => true,
    'public' => true,
    'rewrite' => false,
    'show_in_rest' => true,
  ) );
}

/*
Filters
*/

add_filter ( 'query_vars', 'tv_order_query_vars' );

function tv_order_query_vars ( $vars ) {
    $vars[] .= 'a';
    $vars[] .= 'b';
    return $vars;
}

/*
Classes
*/

class TV_Price extends TV_Post {

  public function __construct ( $post ) {
    parent::__construct ( $post );
  }

}

class TV_Prices extends TV_Query {

  function __construct ( $args = array () ) {
    $args = array_merge ( $args, array (
      'post_type' => 'price',
    ) );
    parent::__construct ( $args );
  }

}
