<?php

/*
Actions
*/

add_action ( 'init', 'tv_area_init' );

function tv_area_init () {
  register_post_type ( 'area', array (
    'labels' => array (
      'name' => 'Areas',
      'singular_name' => 'Area',
      'add_new' => 'Add Area',
      'add_new_item' => 'Add Area',
      'edit_item' => 'Edit Area',
      'new_item' => 'New Area',
      'view_item' => 'View Area',
      'view_items' => 'View Areas',
      'search_items' => 'Search Areas',
      'all_items' => 'Areas',
      'featured_image' => 'Area Photo',
      'set_featured_image' => 'Set Area Photo',
      'remove_featured_image' => 'Remove Area Photo',
      'use_featured_image' => 'Use Area Photo',
      'menu_name' => 'Areas',
      'name_admin_bar' => 'Area',
    ),
    'supports' => array (
      'title',
      'excerpt',
      'editor',
      'thumbnail',
    ),
    'menu_position' => 4,
    'menu_icon' => 'dashicons-location',
    'hierarchical' => true,
    'show_in_menu' => true,
    'public' => true,
    'rewrite' => array (
      'with_front' => false,
    ),
    'show_in_rest' => true,
  ) );
}

/*
Filters
*/

/*
Classes
*/

class TV_Area extends TV_Post {

  public function __construct ( $post ) {
    parent::__construct ( $post );
  }

}

class TV_Areas extends TV_Query {

  function __construct ( $args = array () ) {
    $args = array_merge ( $args, array (
      'post_type' => 'area',
    ) );
    parent::__construct ( $args );
  }

}
