<?php

/*
Actions
*/

add_action ( 'init', 'tv_service_init' );

function tv_service_init () {
  register_post_type ( 'service', array (
    'labels' => array (
      'name' => 'Services',
      'singular_name' => 'Service',
      'add_new' => 'Add Service',
      'add_new_item' => 'Add Service',
      'edit_item' => 'Edit Service',
      'new_item' => 'New Service',
      'view_item' => 'View Service',
      'view_items' => 'View Services',
      'search_items' => 'Search Services',
      'all_items' => 'Services',
      'featured_image' => 'Service Photo',
      'set_featured_image' => 'Set Service Photo',
      'remove_featured_image' => 'Remove Service Photo',
      'use_featured_image' => 'Use Service Photo',
      'menu_name' => 'Services',
      'name_admin_bar' => 'Service',
    ),
    'supports' => array (
      'title',
      'excerpt',
      'editor',
      'page-attributes',
      'thumbnail',
    ),
    'menu_position' => 4,
    'menu_icon' => 'dashicons-clipboard',
    'hierarchical' => true,
    'show_in_menu' => true,
    'public' => true,
    'rewrite' => array (
      'with_front' => false,
    ),
    'show_in_rest' => true,
  ) );
}

/*
Filters
*/

/*
Classes
*/

class TV_Service extends TV_Post {

  public function __construct ( $post ) {
    parent::__construct ( $post );
  }

}

class TV_Services extends TV_Query {

  function __construct ( $args = array () ) {
    $args = array_merge ( $args, array (
      'post_type' => 'service',
    ) );
    parent::__construct ( $args );
  }

}
