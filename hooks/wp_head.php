<?php

/**
 * Initialize the site head
 *
 * @access public
 * @return void
 */
add_action('wp_head', 'site_wp_head');
function site_wp_head() {
	$html = <<<EOD
		<script type="text/javascript" language="javascript">
			jQuery(function($) {
				var opts = {
						ajaxUrl     : '%s',
						siteUrl     : '%s',
						templateUrl : '%s'
					};

				app.init(opts);
				order.init(opts);
			});
		</script>
EOD;

	echo sprintf($html, WP_AJAX_URL, WP_HOME_URL, TEMPLATEDIR);

	$site_css = get_field ( 'css', 'option' );
	$page_css = get_field ( 'css' );
	if ( $site_css || $page_css ) {
		?>
		<style type="text/css">
			<?php echo $site_css; ?>
			<?php echo $page_css; ?>
		</style>
		<?php
	}
}
