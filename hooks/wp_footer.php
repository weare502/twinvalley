<?php

add_action ( 'wp_footer', function () {

	$site_js = get_field ( 'js', 'option' );
	$page_js = get_field ( 'js' );
	if ( $site_js || $page_js ) {
		?>
		<script type="text/javascript">
			<?php echo $site_js; ?>
			<?php echo $page_js; ?>
		</script>
		<?php
	}

} );