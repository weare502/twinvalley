<?php

/**
 * Registers and enqueues site JS and CSS files
 *
 * @access public
 * @return void
 */
add_action('wp_enqueue_scripts', 'site_enqueue_scripts');
add_action('enqueue_block_editor_assets', 'site_enqueue_block_scripts');
function site_enqueue_scripts()
{
	// Register scripts
	wp_register_script('app', sprintf('%s/assets/scripts/app.js', TEMPLATEDIR), false, filemtime(sprintf('%s/assets/scripts/app.js', get_template_directory())));
	wp_register_style('app', sprintf('%s/assets/stylesheets/app.css', TEMPLATEDIR), false, filemtime(sprintf('%s/assets/stylesheets/app.css', get_template_directory())));

	// Enqueue scripts
	wp_enqueue_script('app');
	wp_enqueue_style('app');

	// Style edits from 502 - gulpfile unusable due to outdated version (tries to use graceful-fs) update and re-install do not fix issue - relies in extremely outdated version
	$version = '20000001';
	wp_enqueue_style( 'tv-css', get_stylesheet_directory_uri() . '/static/extra-styles.css', [], $version );
}
function site_enqueue_block_scripts()
{
	// Register scripts
	wp_register_style('blocks', sprintf('%s/assets/stylesheets/blocks.css', TEMPLATEDIR), false, filemtime(sprintf('%s/assets/stylesheets/blocks.css', get_template_directory())));

	// Enqueue scripts
	wp_enqueue_style('blocks');

}
