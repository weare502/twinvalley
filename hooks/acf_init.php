<?php

add_action ( 'acf/init', function () {

    acf_add_options_page ( array (
        'page_title' => 'Twin Valley',
        'menu_title' => 'Twin Valley',
        'menu_slug' => 'twin-valley',
        'capability' => 'edit_pages',
        'redirect' => false,
        'parent_slug' => 'options-general.php',
    ) );

    acf_register_block_type ( array (
        'name' => 'button',
        'title' => 'Button',
        'description' => '',
        'render_template' => 'blocks/button.php',
        'category' => 'tv',
        'icon' => 'admin-links',
        'keywords' => array (
            'button',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'card',
        'title' => 'Card',
        'description' => '',
        'render_template' => 'blocks/card.php',
        'category' => 'tv',
        'icon' => 'tablet',
        'keywords' => array (
            'card',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'eyebrow',
        'title' => 'Eyebrow',
        'description' => '',
        'render_template' => 'blocks/eyebrow.php',
        'category' => 'tv',
        'icon' => 'editor-textcolor',
        'keywords' => array (
            'eyebrow',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'feature',
        'title' => 'Feature',
        'description' => '',
        'render_template' => 'blocks/feature.php',
        'category' => 'tv',
        'icon' => 'star-filled',
        'keywords' => array (
            'feature',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'team',
        'title' => 'Team',
        'description' => '',
        'render_template' => 'blocks/team.php',
        'category' => 'tv',
        'icon' => 'admin-users',
        'keywords' => array (
            'team',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'service',
        'title' => 'Service',
        'description' => '',
        'render_template' => 'blocks/service.php',
        'category' => 'tv',
        'icon' => 'admin-tools',
        'keywords' => array (
            'service',
            'product',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'team',
        'title' => 'Team',
        'description' => '',
        'render_template' => 'blocks/team.php',
        'category' => 'tv',
        'icon' => 'admin-users',
        'keywords' => array (
            'team',
        ),
    ) );

    acf_register_block_type ( array (
        'name' => 'testimonial',
        'title' => 'Testimonial',
        'description' => '',
        'render_template' => 'blocks/testimonial.php',
        'category' => 'tv',
        'icon' => 'format-quote',
        'keywords' => array (
            'testimonial',
            'quote',
        ),
    ) );

} );