<?php

/**
 * Adds the theme support for thumbnail sizes
 *
 * @access public
 * @return void
 */
add_action('after_setup_theme', 'site_after_setup_theme');
function site_after_setup_theme() {

	// Enable theme specific options
	add_theme_support ('title-tag');
	add_theme_support('post-thumbnails');

	// Add alternate image sizes
	add_image_size('default', 770);
	add_image_size('square', 450, 450, true);
	add_image_size('rectangle', 770, 435, true);

	// Registers nav menus
	register_nav_menus([
		'header'       => __('Header', 'tv-header'),
		'footer'       => __('Footer', 'tv-footer'),
		'utility'      => __('Utility', 'tv-utility'),
		'bill_pay'     => __('Mobile Bill Pay', 'tv-bill-pay'),
		'site_info'    => __('Site Info', 'site-info'),
		'quick_links'  => __('Quick Links', 'quick-links'),
	]);

}
