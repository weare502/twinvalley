<?php

use \Ldc\Models\AjaxResponse;

/**
 * Registers a new user
 *
 * @access public
 * @return WP_AjaxResponse
 */
add_action('wp_ajax_submit_order_form', 'site_ajax_submit_order_form');
add_action('wp_ajax_nopriv_submit_order_form', 'site_ajax_submit_order_form');
function site_ajax_submit_order_form()
{
	parse_str($_POST['form_data'], $formData);

	$status   = 'error';
	$response = [
		'message' => 'Something went wrong. Please try again later'
	];

	$packages     = '';
	$upgrades     = '';

	$to           = get_field ('order_form_email', 'option');
	$zip          = esc_attr($formData['zip']);
	$city         = esc_attr($formData['city']);
	$state        = esc_attr($formData['state']);
	$email        = esc_attr($formData['email']);
	$phone        = esc_attr($formData['phone']);
	$subject      = 'New order form submission';
	$address1     = esc_attr($formData['address_1']);
	$address2     = esc_attr($formData['address_2']);
	$timeSlot     = esc_attr($formData['time_slot']);
	$lastName     = esc_attr($formData['last_name']);
	$firstName    = esc_attr($formData['first_name']);

	$packageData  = $_POST['package_data'];
	$upgradeData  = $_POST['upgrade_data'];

	if (!empty($packageData)) {
		foreach ($packageData as $package) {
			$packages .= sprintf('<p><strong>%s</strong> %s</p>', $package['name'], $package['price']);
		}
	}

	if (!empty($upgradeData)) {
		foreach ($upgradeData as $upgrade) {
			$upgrades .= sprintf('<p><strong>%s X(%s)</strong> %s</p>', $upgrade['name'], $upgrade['quantity'], $upgrade['price']);
		}
	}

	$body = <<<EOD
		<html>
			<body>
				<h3>Contact Information</h3>
				<p><strong>Name:</strong> %s %s</p>
				<p><strong>Email:</strong> %s</p>
				<p><strong>Phone Number:</strong> %s</p>
				<p><strong>Address 1:</strong> %s</p>
				<p><strong>Address 2:</strong> %s</p>
				<p>%s %s, %s</p>
				<p><strong>Time Slot:</strong> %s</p>

				<br /><br />

				<h3>Packages</h3>
				%s

				<br /><br />

				<h3>Upgrades</h3>
				%s
			</body>
		</html>
EOD;

	//Format the body
	$bodyFormatted = sprintf(
		$body,
		$firstName,
		$lastName,
		$email,
		$phone,
		$address1,
		$address2,
		$city,
		$state,
		$zip,
		$timeSlot,
		$packages,
		$upgrades
	);

	//Set the headers
	$headers[] = 'From: '.$firstName.' '.$lastName.'<'.$email.'>';

	//Process the mail
	$success = wp_mail($to, $subject, $bodyFormatted, $headers);

	if ($success) {
		$status = 'success';
		$response = [
			'message'  => 'Something went wrong. Please try again later',
			'redirect' => sprintf('%s/thank-you', WP_HOME_URL)
		];
	 }

	$ajaxResponse = new AjaxResponse();
	$ajaxResponse->setStatus($status);
	$ajaxResponse->setResponse($response);
	$ajaxResponse->output();
}
