<?php

/**
 * Set's the wp_mail content type to send out html
 *
 * @access public
 * @param $contentType  string
 * @return void
 */
add_filter('wp_mail_content_type', 'ldc_core_mail_content_type');
function ldc_core_mail_content_type($contentType)
{
	return 'text/html';
}
