<?php

/**
 * Sets the ability to call template includes
 *
 * @access public
 * @return void
 */
add_filter('template_include', 'template_wrapper');
function template_wrapper($template) {
	get_header ();
	include $template;
	get_footer ();
	return false;
}
