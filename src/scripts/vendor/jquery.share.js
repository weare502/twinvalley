/**
 * Share.js
 *
 * @copyright Copyright (c) 2017 Joey Adkins
 * @version 1.0.0
 * @author Joey Adkins <joey@luciddesignconcepts.com>
 * @website http://luciddesignconcepts.com/
 *
 */

var share = (function(x, $) {

	/**
	 * @access private
	 */
	var _endpoint = {
		email : {
			url : 'mailto:?subject={TEXT}&body={IMAGE}{DESC}'
		},
		facebook : {
			url : 'http://www.facebook.com/share.php?u={URL}'
		},
		googleplus : {
			url : 'https://plusone.google.com/_/+1/confirm?hl=en&url={URL}'
		},
		pinterest : {
			url: 'http://pinterest.com/pin/create/button/?url={URL}&media={IMAGE}&description={DESC}'
		},
		tumblr : {
			url : 'http://www.tumblr.com/share?v=3&u={URL}'
		},
		twitter : {
			url : 'https://twitter.com/intent/tweet?url={URL}&text={TEXT}'
		}
	};

	/**
	 * @access private
	 */
	var _events = {

		/**
		 * share toggle onclick callback function
		 *
		 * @param Event e
		 * @return void
		 */
		onClickShareToggle : function(e) {
			e.preventDefault();

			var
				element   = $(this),
				container = element.closest('[data-share-content]'),
				opts      = {
					url     : container.data('share-url'),
					text    : container.data('share-title'),
					desc    : container.data('share-description'),
					image   : container.data('share-image'),
					network : element.data('network')
				};

			_getShareUrl(opts);
		}
	};

	/**
	 * Retrieve the share URL
	 *
	 * @access private
	 * @param object opts
	 * @param string opts.network
	 * @param string opts.url
	 * @param string opts.text
	 * @param string opts.desc
	 */
	var _getShareUrl = function(opts) {
		$.each(_endpoint, function(network, element) {
			if (network === opts.network) {
				var
					url = element.url;

				if (typeof opts.url !== 'undefined') {
					url = url.replace('{URL}', encodeURIComponent(opts.url));
				} else {
					url = url.replace('{URL}', '');
				}

				if (typeof opts.image !== 'undefined') {
					url = url.replace('{IMAGE}', encodeURIComponent(opts.image));
				} else {
					url = url.replace('{IMAGE}', '');
				}

				if (typeof opts.text !== 'undefined') {
					url = url.replace('{TEXT}', encodeURIComponent(opts.text));
				} else {
					url = url.replace('{TEXT}', '');
				}

				if (typeof opts.desc !== 'undefined') {
					url = url.replace('{DESC}', encodeURIComponent(opts.desc));
				} else {
					url = url.replace('{DESC}', '');
				}

				_popup(url);

				return false;
			}
		});
	};

	/**
	 * jQuery document ready callback function
	 *
	 * @access private
	 * @return void
	 */
	var _onDocumentReady = function() {
		$(document)
			// Bind click event to share elements
			.on('click', '.share-toggle', _events.onClickShareToggle);
	};

	/**
	 * Asynchronously load a new JS file into the HTML head
	 *
	 * @access private
	 * @param string file
	 * @return void
	 */
	var _loadJS = function(file) {
		var
			d = document,
			h = d.getElementsByTagName('head')[0],
			s = d.createElement('script');

		s.type = 'text/javascript';
		s.language = 'javascript';
		s.async = true;
		s.src = file;
		h.appendChild(s);
	};

	/**
	 * Handles the popup dialog box
	 *
	 * @access private
	 * @param string url
	 * @return void
	 */
	var _popup = function(url) {
		var
			width  = 600,
			height = 500,
			top    = (screen.height) ? (screen.height - height) / 2 : 0,
			left   = (screen.width) ? (screen.width - width) / 2 : 0;

		window.open(url, '_blank', 'height=' + height + ',width=' + width + ',left=' + left + ',top=' + top + ',location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0');
	};

	$(_onDocumentReady);

	return x;

}(share || {}, jQuery));
