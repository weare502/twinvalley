/**
 * FormRlrmrnts.js
 *
 * @copyright Copyright (c) 2017 Joey Adkins
 * @version 1.0.0
 * @author Joey Adkins <joey@luciddesignconcepts.com>
 * @website http://luciddesignconcepts.com/
 *
 */

if(typeof form === "undefined") { var form = {}; }
var form = (function(form) {

	// !----- Event Callbacks

   /**
	* @access private
	* @var object _events
	*/
	var _events = {

		/**
		 * callback function to switch radio button
		 *
		 * @param object params
		 * @return void
		 */
		onClickRadioButton : function () {
			var
				radio = $(this),
				value = radio.data('value'),
				input = radio.closest('.radio-group').find('input[type="hidden"]');

			input.val(value);
			radio.addClass('checked').siblings().removeClass('checked');
		},

		/**
		 * callback function to switch checkbox
		 *
		 * @param object params
		 * @return void
		 */
		onClickCheckbox : function () {
			var
				checkbox = $(this),
				value    = (checkbox.hasClass('checked')) ? 0 : 1,
				input    = checkbox.find('input[type="hidden"]');

			input.val(value);
			checkbox.toggleClass('checked');
		}
	};


	// !----- Private Methods

	/**
	 * jQuery document ready callback
	 *
	 * @access private
	 * @return void
	 */
	var _onDocumentReady = function () {
		$(document)
			// Bind click event to radio option
			.on('click', '.radio-group li', _events.onClickRadioButton)
			// Bind click event to checkbox option
			.on('click', '.checkbox-group li', _events.onClickCheckbox);
	};


	// !----- Public Methods

	// !----- Bind events
	$(_onDocumentReady);

	// !---- Return the object
	return form;

}(form || {}));
