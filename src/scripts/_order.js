if(typeof order === "undefined") { var order = {}; }
var order = (function(order) {

	/**
	 * @access private
	 * @var object
	 */
	var _opts = {};

	/**
	 * @access private
	 * @var array
	 */
	var _serviceTotalArray = [];

	// !----- Event Callbacks

   /**
	* @access private
	* @var object _events
	*/
	var _events = {

		/**
		 * callback function for window scroll
		 *
		 * @return void
		 */
		onScrollWindow : function () {
			var
				scrollTop = $(this).scrollTop(),
				$summary  = $('.order-form-summary');

			if ($summary.length) {
				if (scrollTop >= ($summary.offset().top - 60)) {
					$summary.find('.order-form-summary-container').addClass('fixed');
				} else {
					$summary.find('.order-form-summary-container').removeClass('fixed');
				}
			}
		},

		/**
		 * callback function for window scroll
		 *
		 * @return void
		 */
		onResizeWindow : function () {
			if ($('.order-form-summary-container').length) {
				$('.order-form-summary-container').css({ width : $('.order-form-summary-container').parent().width() });
			}
		},

		/**
		 * callback function to toggle the form view
		 *
		 * @return void
		 */
		onClickToggleService : function () {
			var $element   = $(this),
				$container = $('[data-order-form]');

			$('html, body').animate({
				scrollTop : 0
			}, 500, function () {
				if ($element.hasClass('btn')) {
					$('.order-form-toggle').attr('data-current-view', $element.data('view')).find('a[data="contact"]').addClass('current-view').siblings().removeClass('current-view');
				} else {
					$element.addClass('current-view').siblings().removeClass('current-view').parent().attr('data-current-view', $element.data('view'));
				}

				$container.attr('data-current-view', $element.data('view'));
			});
		},

		/**
		 * callback function to change the area
		 *
		 * @return void
		 */
		onChangeArea : function () {
			window.location.href = window.location.pathname + '?a=' + $(this).val();
		},

		/**
		 * callback function to change the area
		 *
		 * @return void
		 */
		onChangeAreaModal : function () {
			window.location.href = _opts.siteUrl + '/areas/' + $(this).val() + '/';
		},

		/**
		 * callback function to select a package
		 *
		 * @return void
		 */
		onClickPackageSelect : function () {
			var $element    = $(this),
				packageData = {
					name        : $element.data('package-name'),
					price       : $element.data('package-price'),
					category    : $element.data('package-category'),
					container   : '[data-packaged-service]',
					inputType   : $element.data('input-type'),
					serviceType : $element.data('service-type')
				};

			_updateServiceSummary('package', packageData);
			_toggleUpgradePackages(packageData.serviceType, false);
		},

		/**
		 * callback function to select an upgrade
		 *
		 * @return void
		 */
		onClickUpgradeSelect : function () {
			var $element    = $(this),
				upgradeData = {
					name      : $element.data('upgrade-name'),
					price     : $element.data('upgrade-price'),
					container : '[data-upgrade-service]',
					inputType : $element.data('input-type'),
					groupName : $element.data('group-name'),
					serviceType : $element.data('service-type')
				};

			_updateServiceSummary('upgrade', upgradeData);
		},

		/**
		 * callback function to remove line items
		 *
		 * @return void
		 */
		onClickRemoveLineItem : function (e) {
			e.preventDefault();

			var $element    = $(this),
				container   = $(this).data('container'),
				emptyText   = ('[data-upgrade-service]' === container) ? 'No upgrades selected' : 'No packages selected',
				serviceType = $(this).data('service-type');

			$element.parent().remove();

			if ('[data-packaged-service]' === container) {
				$('[data-upgrade-service]').find('li a[data-service-type="' + serviceType + '"]').each(function (index, element) {
					$(element).parent().remove();
				});

				if (!$('[data-upgrade-service]').find('li').length) {
					$('[data-upgrade-service]').find('ul').html($('<li class="no-selection" data-line-item-price="0.00">' + emptyText + '</li>'));
				}

				_toggleUpgradePackages(serviceType, true);
			}

			if (!$(container).find('li').length) {
				$(container).find('ul').html($('<li class="no-selection" data-line-item-price="0.00">' + emptyText + '</li>'));
			}

			_populateSummaryTotal();
		},

		/**
		 * callback function to submit the order form
		 *
		 * @return void
		 */
		onSubmitOrderForm : function (e) {
			e.preventDefault();

			var $form    = $(this),
				data     = {},
				upgrades = [],
				packages = [];

			$('[data-packaged-service] li').each(function (index, element) {
				if ($(element).hasClass('no-selection')) {
					return false;
				}

				var package = {};

				package.name  = $(element).data('line-item-name');
				package.price = $(element).data('line-item-price');

				packages.push(package);
			});

			$('[data-upgrade-service] li').each(function (index, element) {
				if ($(element).hasClass('no-selection')) {
					return false;
				}

				var upgrade = {};

				upgrade.name     = $(element).data('line-item-name');
				upgrade.price    = $(element).data('line-item-price');
				upgrade.quantity = (typeof $(element).data('line-item-quantity') !== 'undefined') ? $(element).data('line-item-quantity') : 1;

				upgrades.push(upgrade);
			});

			data = {
				action       : 'submit_order_form',
				form_data    : $form.serialize(),
				request_type : $form.attr('method'),
				package_data : packages,
				upgrade_data : upgrades
			};

			if (!_validateForm($form)) {
				alert('You must fill out required fields and select at least 1 package.');
				return false;
			}

			$form.addClass('submitting').find('.btn-submit').addClass('submitting');
			_submitForm(data, function (ret) {
				if (ret.status === 'success') {
					window.location.href = ret.response.redirect;
					gtag_report_conversion('https://twinvalley.net/order');
				} else {
					alert(ret.response.message);
				}
			});
		}
	};


	// !----- Private Methods

	/**
	 * Updates the service summary with the line items and pricing
	 *
	 * @param e
	 * @return void
	 */
	var _updateServiceSummary = function (type, data) {
		var formattedPrice = accounting.formatMoney(data.price, '');

		if ($(data.container).find('li.no-selection').length) {
			$(data.container).find('li.no-selection').remove();
		}

		switch (data.inputType) {
			case 'number':
				if ($(data.container).find('li[data-line-item-name="'+ data.name +'"]').length) {
					var quauntity   = Number($(data.container).find('li[data-line-item-name="'+ data.name +'"]').data('line-item-quantity')),
						newQuantity = quauntity + 1,
						newQuanText = ' (X' + newQuantity + ')';

					$(data.container).find('li[data-line-item-name="'+ data.name +'"]').attr('data-line-item-quantity', newQuantity).data('line-item-quantity', newQuantity).attr('data-line-item-price', accounting.formatMoney((data.price * newQuantity), '')).data('line-item-price', accounting.formatMoney((data.price * newQuantity), '')).html(data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ accounting.formatMoney((data.price * newQuantity), '') +'</span>');
					$('<abbr>'+ newQuanText +'</abbr>').insertBefore($(data.container).find('li[data-line-item-name="'+ data.name +'"] a'));


				} else {
					$(data.container).find('ul').append($('<li data-line-item-name="'+ data.name +'" data-line-item-price="'+ formattedPrice +'" data-line-item-quantity="1">'+ data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ formattedPrice +'</span></li>'));
				}
				break;

			case 'radio':
				if ('package' === type) {
					if ($(data.container).find('li[data-package-category="'+ data.category +'"]').length) {
						$(data.container).find('li[data-package-category="'+ data.category +'"]').replaceWith($('<li data-line-item-name="'+ data.name +'" data-package-category="'+ data.category +'" data-line-item-price="'+ formattedPrice +'">'+ data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ formattedPrice +'</span></li>'));
					} else {
						$(data.container).find('ul').append($('<li data-line-item-name="'+ data.name +'" data-package-category="'+ data.category +'" data-line-item-price="'+ formattedPrice +'">'+ data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ formattedPrice +'</span></li>'));
					}
				} else {
					if ($(data.container).find('li[data-group-name="'+ data.groupName +'"]').length) {
						$(data.container).find('li[data-group-name="'+ data.groupName +'"]').replaceWith($('<li data-line-item-name="'+ data.name +'" data-group-name="'+ data.groupName +'" data-line-item-price="'+ formattedPrice +'">'+ data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ formattedPrice +'</span></li>'));
					} else {
						$(data.container).find('ul').append($('<li data-line-item-name="'+ data.name +'" data-group-name="'+ data.groupName +'" data-line-item-price="'+ formattedPrice +'">'+ data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ formattedPrice +'</span></li>'));
					}
				}

				break;

			case 'checkbox':
			default:
				if (!$(data.container).find('li[data-line-item-name="'+ data.name +'"]').length) {
					$(data.container).find('ul').append($('<li data-line-item-name="'+ data.name +'" data-line-item-price="'+ formattedPrice +'">'+ data.name +' <a href="#" data-service-type="' + data.serviceType + '" data-container="' + data.container + '" data-remove-line-item>Remove</a><span>$'+ formattedPrice +'</span></li>'));
				}
				break;
		}

		_populateSummaryTotal();
	};

	/**
	 * Populates the summary total
	 *
	 * @param e
	 * @return void
	 */
	var _populateSummaryTotal = function () {
		var	d,c,
			array,
			string,
			lItems   = [],
			$cents   = $('.summary-total').find('.cents'),
			$dollars = $('.summary-total').find('.dollars');

		$('.order-form-summary li').each(function (index, element) {
			lItems.push(Number($(element).data('line-item-price')));
		});

		sum    = lItems.reduce((a, b) => a + b, 0);
		format = accounting.formatMoney(sum, '');
		array  = format.split('.');

		$dollars.html(array[0]);
		$cents.html(array[1]);
	};

	/**
	 * Toggles the upgrade packages that are tied to the selected package
	 *
	 * @param e
	 * @return void
	 */
	var _toggleUpgradePackages = function (service, removeItem) {
		var	$parent      = $('.order-form-upgrade-block');
			serviceArray = service.split(',');

		$parent.show();

		$('.order-form-upgrade-block .order-form-option-block').each(function (index, element) {
			var service = $(element).data('service');

			if (serviceArray.includes(service)) {
				if (removeItem && $(element).is(':visible')) {
					$(element).hide();
				} else {
					$(element).show();
				}
			}
		});

	};

	/**
	 * Validates the form
	 *
	 * @access private
	 * @param data object
	 * @param callback function (optional)
	 * @return void
	 */
	var _validateForm = function (form) {
		if (!$('[data-packaged-service]').find('li.no-selection').length) {
			if ($('[name="first_name"]', form).val() != '' || $('[name="last_name"]', form).val() != '' || $('[name="email"]', form).val() != '') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	};

	/**
	 * Submits the forms
	 *
	 * @access private
	 * @param data object
	 * @param callback function (optional)
	 * @return void
	 */
	var _submitForm = function (data, callback) {
		console.log(data);
		$.ajax({
			url      : _opts.ajaxUrl,
			data     : data,
			type     : (data.request_type) ? data.request_type : 'GET',
			success  : callback,
			dataType : (data.data_type) ? data.data_type : 'json'
		});
	};

	/**
	 * jQuery document ready callback
	 *
	 * @access private
	 * @return void
	 */
	var _onDocumentReady = function () {
		$(document).ready(function(){
			// Pre-select the package if provided
			var params = new URLSearchParams(window.location.search);
			if(params.has('b')){
				button = $('button[data-package-slug="' + params.get('b') + '"');
				$(button).click();
			}
		});

		$(window)
			// Bind scroll event to window
			.on('scroll', _events.onScrollWindow)
			// Bind resize event to window
			.on('resize', _events.onResizeWindow);

		$(document)
			// Bind click event to toggle view of order form
			.on('click', '[data-toggle-view]', _events.onClickToggleService)
			// Bind click event to select service package
			.on('click', '[data-package-select]', _events.onClickPackageSelect)
			// Bind click event to select service upgrade
			.on('click', '[data-upgrade-select]', _events.onClickUpgradeSelect)
			// Bind click event to remove from order summary
			.on('click', '[data-remove-line-item]', _events.onClickRemoveLineItem)
			// Bind =change event to change service area
			.on('change', '#order-area', _events.onChangeArea)
			// Bind =change event to change service area
			.on('change', '#modal-area', _events.onChangeAreaModal)
			// Bind =change event to change service area
			.on('submit', '.order-form', _events.onSubmitOrderForm);

		if ($('.order-form-summary-container').length) {
			$('.order-form-summary-container').css({ width : $('.order-form-summary-container').parent().width() });
		}
	};


	// !----- Public Methods

   /**
	* Initializes the order object
	*
	* @access public
	* @return void
	*/
	order.init = function (opts) {
		_opts = $.extend({}, _opts, opts);
	};

	// !----- Bind events
	$(_onDocumentReady);

	// !---- Return the object
	return order;

}(order || {}));
