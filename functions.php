<?php

//Define default constants
define('WP_HOME_URL', get_home_url());
define('WP_SITENAME', get_bloginfo('name'));
define('WP_DESCRIPTION', get_bloginfo('description'));
define('WP_AJAX_URL', admin_url('admin-ajax.php'));
define('TEMPLATEDIR', str_replace(WP_CONTENT_DIR, '', WP_CONTENT_URL . TEMPLATEPATH));
define('STYLESHEET_URL', get_stylesheet_directory_uri());
define('TEMPLATE_URL', get_template_directory_uri());
define('MF_UPLOADS_DIR', sprintf('%s/wp-content/files_mf/', WP_HOME_URL));
define('WP_UPLOADS_DIR', sprintf('%s/wp-content/uploads', WP_HOME_URL));
define('RSS2_URL', get_bloginfo('rss2_url'));
define('TRANSIENT_TIMEOUT', 900);

// Iclude Classes
include ('classes/TV_Post.php');
include ('classes/TV_Query.php');
include ('classes/TV_Area.php');
include ('classes/TV_Service.php');
include ('classes/TV_Price.php');

// Include Models
include ('models/View.php');
include ('models/Model.php');
include ('models/AjaxResponse.php');

// Auto-load hooks
$hooks = glob(TEMPLATEPATH.'/hooks/*');
if (count($hooks)) {
	foreach($hooks as $hook) {
		if (file_exists($hook) && is_file($hook)) { require_once($hook); }
	}
}

// Gutenberg
add_theme_support ( 'align-wide' );
add_filter ( 'block_categories', function ( $categories, $post ) {
	$categories[] = array (
		'slug' => 'tv',
		'title' => 'Twin Valley',
	);
	return $categories;
}, 10, 2);
add_filter ( 'allowed_block_types', function ( $allowed_block_types ) {
	return array (
		'core/block',
		'core/heading',
		'core/paragraph',
		'core/image',
		'core/columns',
		'core/separator',
		'core/spacer',
		'core/html',
		'acf/button',
		'acf/card',
		'acf/eyebrow',
		'acf/feature',
		'acf/profile',
		'acf/service',
		'acf/team',
		'acf/testimonial',
		'gravityforms/form',
	);
} );
