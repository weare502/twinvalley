<?php

namespace Ldc\Models;

/**
 *  View
 *
 * @package Models
 * @subpackage View
 * @static
 * @version 1.0.0
 */
class View
{
	/**
	 * Retrieve's and output the view template
	 *
	 * View files are located in the active WordPress theme directory.
	 *
	 * @access public
	 * @static
	 * @param string $name          The name of the view to load (relative to the theme directory)
	 * @param array $vars           A key-value paired array of variables to pass into the view
	 * @return string
	 */
	public static function get($name, $vars = []) {
		extract($vars);

		$html = '';

		$filename = locate_template(sprintf('%s.php', $name));

		if ($filename) {
			ob_start();
			include($filename);
			$html = ob_get_contents();
			ob_end_clean();
		}

		return $html;
	}

}
