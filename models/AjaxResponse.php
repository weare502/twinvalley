<?php

namespace Ldc\Models;

/**
 * The AJAX Response model
 *
 * @package Models
 * @subpackage AjaxResponse
 * @static
 * @version 1.0.0
 */
class AjaxResponse extends Model
{

	/**
	 * Creates a new AJAX response
	 *
	 * @access public
	 * @param string $status
	 * @param array $response
	 */
	public function __construct($status = 'error', $response = [])
	{
		$this->setStatus($status);
		$this->setResponse($response);
	}

	/**
	 * Sets the status
	 *
	 * @access public
	 * @param string $status
	 * @return void
	 */
	public function setStatus($status)
	{
		$this->_status = $status;
	}

	/**
	 * Sets the response
	 *
	 * @access public
	 * @param array $response
	 * @return void
	 */
	public function setResponse($response)
	{
		$this->_response = $response;
	}

	/**
	 * Outputs the JSON response
	 *
	 * @access public
	 * @return void
	 */
	public function output()
	{
		header('Content-type: application/json');

		$data = [
			'status' => $this->_status,
			'response' => $this->_response
  		];

		echo json_encode($data);
		die();
	}
}
